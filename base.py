from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Dict, Iterable, Optional


class AbstractProvider(ABC):
    _CATEGORY: str


@dataclass(frozen=True)
class EventProviderContext:
    pass


class ContextBuilder:
    def build(self, context: Dict) -> Optional[EventProviderContext]:
        return None


class CategoryProvider(AbstractProvider):
    @abstractmethod
    def get_emails(self) -> Optional[Iterable[Dict]]:
        raise NotImplementedError


class EventProvider(AbstractProvider):
    @abstractmethod
    def get_email(
        self,
    ) -> Optional[Dict]:
        raise NotImplementedError
