from enum import Enum


class Category(Enum):
    COMPOSITION = "Composition"
