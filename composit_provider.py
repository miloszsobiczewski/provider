from dataclasses import dataclass
from typing import Dict, Iterable, Optional

from .base import CategoryProvider, ContextBuilder, EventProvider, EventProviderContext
from .services.email_sender import EmailSender, email_sender


@dataclass(frozen=True)
class CompositionProviderContext(EventProviderContext):
    event: str


class CompositionProviderContextBuilder(ContextBuilder):
    def build(self, context: Dict):
        return CompositionProviderContext(event=context["foo"])


class CompositionProvider(EventProvider, CategoryProvider):
    def __init__(self, email_sender: EmailSender):
        super().__init__()

        self._email_sender = email_sender

    def get_email(self):
        email = {"foo": "bar"}
        self._email_sender.send("email", email)
        return email

    def get_emails(self) -> Optional[Iterable[Dict]]:
        emails = [{"foo": "bar"}]
        [self._email_sender.send("email", email) for email in emails]
        return emails


provider = CompositionProvider()
builder = CompositionProviderContextBuilder()
