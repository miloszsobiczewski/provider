class UnsupportedCategory(Exception):
    def __init__(self, category: str):
        super().__init__(f"Following category: '{category}' is not supported")
