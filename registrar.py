from typing import Dict, Optional, Type

from .base import CategoryProvider, ContextBuilder, EventProvider, EventProviderContext
from .categories import Category
from .composit_provider import CompositionProviderContext
from .composit_provider import builder as composition_builder
from .composit_provider import provider as composition_provider
from .exceptions import UnsupportedCategory


class EventProviderRegistrar:
    _PROVIDER: Dict[str, EventProvider] = {}

    def register(
        self, category: str, provider: EventProvider
    ) -> "EventProviderRegistrar":
        self._PROVIDER[category] = provider
        return self

    def get(self, category: str) -> EventProvider:
        scheduler = self._PROVIDER.get(category)

        if scheduler is None:
            raise UnsupportedCategory(category)

        return scheduler


event_provider_registrar = EventProviderRegistrar().register(
    Category.COMPOSITION.value,
    composition_provider,
)


class CategoryProviderRegistrar:
    _PROVIDER: Dict[str, CategoryProvider] = {}

    def register(
        self, category: str, scheduler: CategoryProvider
    ) -> "CategoryProviderRegistrar":
        self._PROVIDER[category] = scheduler
        return self

    def get(self, category: str) -> CategoryProvider:
        scheduler = self._PROVIDER.get(category)

        if scheduler is None:
            raise UnsupportedCategory(category)

        return scheduler


category_scheduler_registrar = CategoryProviderRegistrar().register(
    Category.COMPOSITION.value,
    composition_provider,
)


class EventContextRegistrar:
    _CONTEXTS: Dict[str, Type[EventProviderContext]] = {}
    _DEFAULT_CONTEXT: Type[EventProviderContext] = EventProviderContext
    _BUILDER: Dict[str, ContextBuilder] = {}
    _DEFAULT_BUILDER: ContextBuilder = ContextBuilder()

    def register(
        self,
        category: str,
        context: Type[EventProviderContext],
        builder: ContextBuilder,
    ) -> "EventContextRegistrar":
        self._CONTEXTS[category] = context
        self._BUILDER[category] = builder
        return self

    def get(self, category: str) -> Type[EventProviderContext]:
        return self._CONTEXTS.get(category, self._DEFAULT_CONTEXT)

    def build(self, category: str, context: Dict) -> Optional[EventProviderContext]:
        builder = self._BUILDER.get(category, self._DEFAULT_BUILDER)
        return builder.build(context)


event_context_registrar = EventContextRegistrar().register(
    Category.COMPOSITION.value,
    CompositionProviderContext,
    composition_builder,
)
